package com.example.glownd.office_soundboard;

import android.graphics.Color;
import android.graphics.ColorFilter;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.media.MediaPlayer;
import android.view.View;
import android.widget.Button;
import android.graphics.PorterDuff;




public class Soundboard_Main extends AppCompatActivity {

    MediaPlayer[] my_players = new MediaPlayer[10];
    Button[] my_buttons = new Button[10];

    public boolean media_is_playing = false;
    public MediaPlayer last_player = null;
    public Button last_button = null;


    public int [] mp_ids =
            {
                    R.raw.creed_bankrupcy,
                    R.raw.creed_bobody,
                    R.raw.creed_cults,
                    R.raw.creed_homeless_man,
                    R.raw.creed_new_chair,
                    R.raw.creed_pretend_talking,
                    R.raw.creed_scuba,
                    R.raw.creed_thoughts,
                    R.raw.creed_sixties
            };


    public int [] button_ids =
            {
                    R.id.sound_button1,
                    R.id.sound_button2,
                    R.id.sound_button3,
                    R.id.sound_button4,
                    R.id.sound_button5,
                    R.id.sound_button6,
                    R.id.sound_button7,
                    R.id.sound_button8,
                    R.id.sound_button9
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soundboard__main);

        /*
        final MediaPlayer MP_creed_bankrupcy = MediaPlayer.create(this, R.raw.creed_bankrupcy);
        final MediaPlayer MP_creed_bobody = MediaPlayer.create(this, R.raw.creed_bobody);
        final MediaPlayer MP_creed_cults = MediaPlayer.create(this, R.raw.creed_cults);
        final MediaPlayer MP_creed_homeless_man = MediaPlayer.create(this, R.raw.creed_homeless_man);
        final MediaPlayer MP_creed_new_chair = MediaPlayer.create(this, R.raw.creed_new_chair);
        final MediaPlayer MP_creed_pretend_talking = MediaPlayer.create(this, R.raw.creed_pretend_talking);
        final MediaPlayer MP_creed_scuba = MediaPlayer.create(this, R.raw.creed_scuba);
        final MediaPlayer MP_creed_sixties = MediaPlayer.create(this, R.raw.creed_sixties);
        final MediaPlayer MP_creed_thoughts = MediaPlayer.create(this, R.raw.creed_thoughts);


        my_players[0] = MP_creed_bankrupcy;
        my_players[1] = MP_creed_bobody;
        my_players[2] = MP_creed_cults;
        my_players[3] = MP_creed_homeless_man;
        my_players[4] = MP_creed_new_chair;
        my_players[5] = MP_creed_pretend_talking;
        my_players[6] = MP_creed_scuba;
        my_players[7] = MP_creed_sixties;
        my_players[8] = MP_creed_thoughts;

        final Button play_sound1 = (Button) this.findViewById(R.id.sound_button1);
        final Button play_sound2 = (Button) this.findViewById(R.id.sound_button2);
        final Button play_sound3 = (Button) this.findViewById(R.id.sound_button3);
        final Button play_sound4 = (Button) this.findViewById(R.id.sound_button4);
        final Button play_sound5 = (Button) this.findViewById(R.id.sound_button5);
        final Button play_sound6 = (Button) this.findViewById(R.id.sound_button6);
        final Button play_sound7 = (Button) this.findViewById(R.id.sound_button7);
        final Button play_sound8 = (Button) this.findViewById(R.id.sound_button8);
        final Button play_sound9 = (Button) this.findViewById(R.id.sound_button9);

        my_buttons[0] = play_sound1;
        my_buttons[1] = play_sound2;
        my_buttons[2] = play_sound3;
        my_buttons[3] = play_sound4;
        my_buttons[4] = play_sound5;
        my_buttons[5] = play_sound6;
        my_buttons[6] = play_sound7;
        my_buttons[7] = play_sound8;
        my_buttons[8] = play_sound9;
        */

        set_listeners_MP3(mp_ids, button_ids);
        /*
        play_sound7.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if(MP_creed_scuba.isPlaying())
                {
                    MP_creed_scuba.pause();
                    MP_creed_scuba.seekTo(0);
                    play_sound7.setBackgroundColor(Color.GRAY);
                }
                else
                {
                    MP_creed_scuba.start();
                    play_sound7.setBackgroundColor(Color.BLUE);
                }

            }
        });

        MP_creed_scuba.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                play_sound7.setBackgroundColor(Color.GRAY);
            }
        });

        */
    }

    public void set_listeners_MP3(int [] mp_ids, int [] button_ids)
    {
        for(int i=0; i<button_ids.length; i++)
        {
            final MediaPlayer current_player =  MediaPlayer.create(this, mp_ids[i]);
            final Button current_button = (Button) findViewById(button_ids[i]);
            final ColorFilter standard_grey = current_button.getBackground().getColorFilter();

            current_player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    current_button.getBackground().setColorFilter(standard_grey);
                }
            });

            current_button.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                    if(current_player.isPlaying())
                    {
                        current_player.pause();
                        current_player.seekTo(0);
                        media_is_playing = false;
                        current_button.getBackground().setColorFilter(standard_grey);
                    }
                    else
                    {
                        if(last_player != null && last_button != null)
                        {
                            last_button.getBackground().setColorFilter(standard_grey);
                            last_player.pause();
                            last_player.seekTo(0);
                        }

                        current_player.start();
                        media_is_playing = true;
                        current_button.getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);
                    }

                    last_player = current_player;
                    last_button = current_button;
                }
            });
        }
    }
}
